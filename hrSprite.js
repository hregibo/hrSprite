/**
 * An easy animator for spritesheets. By default if a param h or w isnt given, it will take the same as the given one (square images).
 * Please note, your target element should have already the aspect-ratio of a frame / sprite. It will look horrible otherwise.
 * See the repo for updates, and pull requests - https://gitlab.com/hregibo/hrSprite
 * @param {Object} options the options to pass to the animator. Mandatory:
 * frame_w or frame_h - defining the size of a frame from a sprite
 * source_w or source_h - the size of the given source image
 * source - the image source to inject as a spritesheet
 * target - the DomElement (using document.querySelector for example) of the element to use. If not a canvas, please display: block the element.
 * rows or cols - the number of rows and columns in the spritesheet.
 * delay - optional, the delay in milliseconds. Defaults to 50ms
 */
var hrSprite = function hrSprite(options) {
    this.opt = {
        cols: null,
        rows: null,
        frame_h: null,
        frame_w: null,
        source_h: null,
        source_w: null,
        source_href: null,
        verbose: false,
    };
    this.animation = {
        current_col: 0,
        current_row: 0,
        frame: 0,  
        frames: 1,
        target: null,
        target_w: 420,
        target_h: 420,
        interval: null,
        delay: null,
    }
    
    if (!options.target) {
        console.error("hrSprite: Missing 'target' parameter: a target is required");
        return null;
    }
    if (!options.source) {
        console.error("hrSprite: Missing 'source' parameter: a source (path) is required");
        return null;
    }
    if(options.verbose) {
        this.opt.verbose = options.verbose;
    }
    this.animation.delay = options.delay;

    // precalculate frame size
    if (!options.frame_w && options.source_w && options.cols) {
        // claculate width
        options.frame_w = options.source_w / options.cols;
    }
    if (!options.frame_h && options.source_h && options.rows) {
        // claculate height
        options.frame_h = options.source_h / options.rows;
    }
    // precalculate file size
    if (options.frame_w && !options.source_w && options.cols) {
        // claculate width
        options.source_w = options.frame_w * options.cols;
    }
    if (options.frame_h && !options.source_h && options.rows) {
        // claculate height
        options.source_h = options.frame_h * options.rows;
    }
    // precalculate cols and rows
    if (options.frame_w && options.source_w && !options.cols) {
        // claculate height
        options.cols = options.source_w / options.frame_w;
    }
    if (options.frame_h && options.source_h && !options.rows) {
        // claculate height
        options.rows = options.source_h / options.frame_h;
    }

    if (!options.frame_w && !options.frame_h) {
        console.error("hrSprite: at lease one of the following parameters is required: frame_w, frame_h", options.frame_w, options.frame_h);
        return null;
    }
    if (!options.source_w && !options.source_h) {
        console.error("hrSprite: at lease one of the following parameters is required: source_w, source_h");
        return null;
    }
    if (!options.target_w && !options.target_h) {
        console.error("hrSprite: at lease one of the following parameters is required: target_w, target_h");
        return null;
    }
    if (!options.delay) {
        options.delay = 1000 / 20;
    }

    // Setting source image
    this.opt.source_href = options.source;
    // setting a single frame width & height
    if (!options.frame_w || !options.frame_h) {
        if(options.frame_w) {
            this.opt.frame_h = options.frame_w;
            this.opt.frame_w = options.frame_w;
        } else {
            this.opt.frame_h = options.frame_h;
            this.opt.frame_w = options.frame_h;
        }
    } else {
        this.opt.frame_h = options.frame_h;
        this.opt.frame_w = options.frame_w;
    }
    // setting source height and width
    if (!options.source_w || !options.source_h) {
        if(options.source_w) {
            this.opt.source_h = options.source_w;
            this.opt.source_w = options.source_w;
        } else {
            this.opt.source_h = options.source_h;
            this.opt.source_w = options.source_h;
        }
    } else {
        this.opt.source_h = options.source_h;
        this.opt.source_w = options.source_w;
    }
    // settings cols & rows
    if (!options.cols || !options.rows) {
        if(options.cols) {
            this.opt.rows = options.cols;
            this.opt.cols = options.cols;
        } else {
            this.opt.rows = options.rows;
            this.opt.cols = options.rows;
        }
    } else {
        this.opt.rows = options.rows;
        this.opt.cols = options.cols;
    }
    // settings cols & rows
    if (!options.target_w || !options.target_h) {
        if(options.target_w) {
            this.animation.target_h = options.target_w;
            this.animation.target_w = options.target_w;
        } else {
            this.animation.target_h = options.target_h;
            this.animation.target_w = options.target_h;
        }
    } else {
        this.animation.target_h = options.target_h;
        this.animation.target_w = options.target_w;
    }
    // calculate frames
    var _per_row_frames = this.opt.source_w / this.opt.frame_h;
    var _line_count     = this.opt.source_h / this.opt.frame_h;
    this.animation.frames = _line_count * _per_row_frames;
    // Target
    this.animation.target = options.target;
    this.animation.target.style['background-position'] = "0px 0px";
    this.animation.target.style['background-image'] = "url("+this.opt.source_href+")";
    this.animation.target.style['background-size'] = Math.floor((this.animation.target_w/this.opt.frame_w)*this.opt.source_w)+"px " + Math.floor((this.animation.target_h/this.opt.frame_h)*this.opt.source_h)+"px";
    return this;
};
hrSprite.prototype.pause = function() {
    if(this.animation.interval) {
        clearInterval(this.animation.interval);
    }
}
hrSprite.prototype.resume = function() {
    var context = this;
    this.animation.interval = setInterval(function() {
        context.renderer();
    }, this.animation.delay);
}
hrSprite.prototype.renderer = function() {
    this.animation.current_col = (this.animation.frame % this.opt.cols) +1;
    this.animation.current_row = Math.ceil((this.animation.frame+1) / this.opt.rows);
    
    var _left_padding = (this.animation.current_col-1) * this.animation.target_w;
    var _top_padding  = (this.animation.current_row-1) * this.animation.target_h;
    
    if (this.opt.verbose) {
        console.log('frame', this.animation.frame + 1, 'out of', this.animation.frames, 
        'row',   this.animation.current_row,     'col', this.animation.current_col,
        'bg-offset-x',  _left_padding,    'bg-offset-y', _top_padding);
    }
    
    this.animation.target.style['background-position'] = "-" + _left_padding + "px -"+_top_padding+"px";
    this.animation.frame++;
    if(this.animation.frame >= this.animation.frames) {
        this.animation.frame = 0;
    }
}